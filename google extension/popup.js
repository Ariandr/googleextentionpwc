function getCurrentTabUrl(callback) {
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function (tabs) {
    var tab = tabs[0];
    var url = tab.url;
    console.assert(typeof url == 'string', 'tab.url should be a string');
    callback(url);
  });
}

function sendBookmark(addTerm, errorCallback) {
  var data = {
    url: addTerm
  };
  renderStatus("Adding: " + addTerm);
  var postUrl = 'http://pwcapp.azurewebsites.net/bookmarks/extension/add';
  var x = new XMLHttpRequest();
  x.open('POST', postUrl, true);
  x.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  x.onreadystatechange = function () {
    if (x.readyState == 4) {
      if (x.status == 200) {
        renderStatus('Bookmark was added!');
      } else {
        renderStatus('Error! Please, Log in!');
      }
    }
  };
  x.onerror = function () {
    errorCallback('Network error');
  };
  x.send(JSON.stringify(data));
}

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;
}

document.addEventListener('DOMContentLoaded', function () {
  getCurrentTabUrl(function (url) {
    var links = document.getElementsByTagName("a");
    for (var i = 0; i < links.length; i++) {
      (function () {
        var ln = links[i];
        var location = ln.href;
        ln.onclick = function () {
          chrome.tabs.create({ active: true, url: location });
        };
      })();
    }
    document.getElementById('addButton').addEventListener('click', function(){
      sendBookmark(url, function (errorMessage) {
      renderStatus('Cannot send bookmark ' + errorMessage);
    });
    });
  });
});
